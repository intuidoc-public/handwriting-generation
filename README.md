# Synthetic Handwriting Generation

COMING SOON !
I just have to take a one-week break, and I will start cleaning, updating, and posting the code here!

This repository will contain code to generate realistic looking handwritings.
It could be applied to generate either modern or historical synthetic text-lines.

It will include the following:
- Installation instructions
- Source code for the generators
- Instructions for generation, along with examples
- Some existing scenarios, along with every parameters (fonts, augmentations, ...)
